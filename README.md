# Korean Labyrinth

## Execute

```bash
pip3 install -r requirements.txt
python3 run_game.py
```

## Convert to exe

```powershell
pip3 install pyinstaller
pyinstaller.exe --add-data "resources;resources" run_game.py
```
