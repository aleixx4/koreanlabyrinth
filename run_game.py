"""
Starting point
"""

import os
import sys
import pygame
from params import SCREEN_WIDTH, SCREEN_HEIGTH, FPS
from scene import TitleScene, InstructionsScene, TransitionScene


def run_game():
    """ Main logic
    """
    os.environ["SDL_VIDEO_CENTERED"] = "1"
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))
    pygame.display.set_caption("Get to the red square!")
    clock = pygame.time.Clock()

    starting_scene = TitleScene(screen)
    active_scene = starting_scene

    while active_scene is not None:
        pressed_keys = pygame.key.get_pressed()

        # Event filtering
        filtered_events = []
        for event in pygame.event.get():
            quit_attempt = False
            if event.type == pygame.QUIT:
                quit_attempt = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    if not isinstance(
                        active_scene, (InstructionsScene, TransitionScene)
                    ):
                        quit_attempt = True

                elif event.key == pygame.K_F4 and pressed_keys[pygame.K_LALT]:
                    quit_attempt = True

            if quit_attempt:
                pygame.quit()
                sys.exit()
            else:
                filtered_events.append(event)

        active_scene.process_input(filtered_events, pressed_keys)
        active_scene.update()
        active_scene.render()

        active_scene = active_scene.next

        # pygame.display.flip()
        clock.tick(FPS)


run_game()
